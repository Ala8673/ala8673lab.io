---
title: cmake
date: 2024-01-02 23:24:51
tags: 
    - cmake
    - example
categories:
    - 常用工具
---


## Examples

### 1. 遍历文件夹下所有cpp文件
```c++
cmake_minimum_required(VERSION 3.27)
project(leetcode)

set(CMAKE_CXX_STANDARD 17)

# 遍历文件
# GLOBAL_RECURSE 递归遍历，会遍历normal、hard、easy目录下所有cpp文件
# GLOB 普通遍历
file(GLOB_RECURSE SOURCE_FILES "normal/*.cpp" "hard/*.cpp" "easy/*.cpp" "main.cpp")

# 循环操作文件
foreach (SOURCE_FILE ${SOURCE_FILES})
    get_filename_component(SOURCE_FILE_NAME ${SOURCE_FILE} NAME_WE)
    add_executable(${SOURCE_FILE_NAME} ${SOURCE_FILE})
endforeach ()

```