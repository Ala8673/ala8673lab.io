---
title: C++类型转换
date: 2024-01-18 16:27:10
tags:
    - c/c++
categories:
    - c/c++
    - 基础知识

---

## 1. C-Style 类型转换

```c++
// 这种称之为隐式的类型转换，编译器默默地将int类型的变量进行了转换
int a = 1;
double b = a；
// 或者说，这样，稍微显式一些的类型转换
// 这种转换会导致信息丢失
int c = (int)(5.0)；
```

任何指针的隐式转换都是无效的，除了以下几种情况

+ 从派生类到基类的指针转换
+ 从void*到目标类型的指针转换
+ 从任何指针到void*的转换
+ 从任何指针类型到const void*的转换

## 2. C++-Style 类型转换

### 2.1. static_cast

用途

+ 基本类型转换

```c++
int a = 1;
double b = static_cast<double>(a);
```


<font style="color: red">

Attention: 

static_cast 不对转换做检查

+ static_cast不能去掉表达式的const性质
+ static_cast不能去掉表达式的volatile性质
+ 不要将static_cast用于复杂类型转换，容易产生错误

</font>

### 2.2. const_cast

用途

+ a) 去除const或volatile属性

```c++
const int a = 1;
int b = const_cast<int>(a);
```

### 2.3. dynamic_cast

用途

+ 用于类层次间的上行转换和下行转换，但在转换时会先进行类型检查，需要转换的类型必须是多态类型（即含有虚函数的类），转换失败时，返回空指针.
+ 需要有虚表的原因：类中存在虚表，就说明它有想要让基类指针或引用指向派生类对象的情况，dynamic_cast认为此时转换才有意义（事实也确实如此）。而且dynamic_cast运行时的类型检查需要有运行时类型信息，这个信息是存储在类的虚表中的

### 2.4. reinterpret_cast

reinterpret_cast 表达式不会编译成任何 CPU 指令（除非在整数和指针间转换，或在指针表示依赖其类型的不明架构上）。它纯粹是一个编译时指令，指示编译器将 表达式 视为如同具有 新类型 类型一样处理。
