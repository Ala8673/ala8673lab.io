---
title: Manacher算法（回文串统计）
date: 2024-11-25 19:13:20
tags:
    - c/c++
    - 算法
    - Manacher
    - 回文串
categories:
    - 算法
mathjax: true
---

# Manacher算法（回文串统计）

## 代码

### 统一求解d1和d2

将 `s = abababc` 转换为 `s = #a#b#a#b#a#b#c#`，这样可以通过求解 d1 来得到 d1 和 d2 的值。


### 分开求解d1和d2

+ `         int k = i > r ? 1 : min(d1[l + r - i], r - i + 1); `
    + 这里得先明确，因为 `i` 为中间点, `l` 和 `r` 为回文串的左右端点，所以有 $i \ge \frac{l+r}{2}$
    + $l + r - i = \frac{l+r}{2} - (i - \frac{l+r}{2})$ , `i`关于区间`[l, r]`的中点的对称点
    + 已知的信息，即 `[l, r]` 和 `[0, i)` 的信息，无法用于预测 `r` 以后的位置，所以 $k \le r - i + 1$
```c++
vector<int> manacher(const string &s) {
    vector<int> d1(s.length());
    // 枚举的 i 实际上为 回文串区间的中心点
    for (int i = 0, l = 0, r = -1; i < s.length(); i++) {
        // l, r 表示当前最长回文串的左右边界， s[l, r] 为回文串
        // 如果 i <= r，那么考虑在 [l, r] 内 i 的对称点 i' = l + r - i， 起码在 [l, r] 内，i' 的回文半径 
        int k = i > r ? 1 : min(d1[l + r - i], r - i + 1);

        // 朴素求解
        while (i - k >= 0 && i + k < s.length() && s[i - k] == s[i + k]) {
            k++;
        }
        d1[i] = k--;
        if (i + k > r) {
            l = i - k;
            r = i + k;
        }
    }

    return d1;
}
```

```c++
vector<int> manache(const string& s) {
    vector<int> d2(n);
    for (int i = 0, l = 0, r = -1; i < n; i++) {
        int k = (i > r) ? 0 : min(d2[l + r - i + 1], r - i + 1);
        while (0 <= i - k - 1 && i + k < n && s[i - k - 1] == s[i + k]) {
            k++;
        }
        d2[i] = k--;
        if (i + k > r) {
            l = i - k - 1;
            r = i + k;
       }
    }
}
```


