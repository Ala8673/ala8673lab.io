---
title: 扩展kmp（z算法）
date: 2024-11-25 18:55:28
tags:
    - c/c++
    - 算法
    - 扩展KMP
    - Z算法
    - 字符串匹配
categories:
    - 算法
---


# 扩展kmp（z算法）

## 和kmp的区别

kmp：next数组的含义——前i项的最长公共前后缀长度
扩展kmp：z数组的含义——以i为起点的串与以0为起点的串的最长公共前缀长度

## z数组构建算法

```c++
vector<int> build(const string &s) {
    vector<int> z(s.length());

    // l, r 表示当前最长公共前缀的左右边界 —— s[l, r] == s[0, r - l]
    // 如果 i <= r，那么 s[i] == s[i - l]，所以 z[i] = z[i - l]
    // 否则，暴力求解
    for (int i = 1, l = 0, r = 0; i < s.length(); i++) {
        // 利用已知的z数组的值，减少重复计算
        if (i <= r && z[i - l] < r - i + 1) {
            z[i] = z[i - l];
        } else {
            z[i] = max(0, r - i + 1);
            // 以i为起点的串与以0为起点的串的最长公共前缀长度，朴素求解
            while (i + z[i] < s.length() && s[z[i]] == s[i + z[i]]) {
                z[i]++;
            }
        }

        if (i + z[i] - 1 > r) {
            l = i;
            r = i + z[i] - 1;
        }
    }
    return z;
}
```

## 用法

s 为文本串，pat 为模式串，z 为 z 数组，pat + s 为新串

此时，当 z[i] == pat.length() 时，表示 pat 与 s[i, i + pat.length() - 1] 匹配， 可用于字串匹配


