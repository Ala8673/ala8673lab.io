---
title: Gitlab CI/CD
date: 2023-12-01 15:37:09
tags: 
    - gitlab
    - CI/CD
categories: 
    - 常用工具
---

当前部分主要针对 `.gitlab-ci.yml` 文件

## 1. gitlab-ci 文件属性

| 属性          | 含义                                                                                             |
|---------------|--------------------------------------------------------------------------------------------------|
| stages        | 定义任务内的阶段，每个阶段必须从全局定义的阶段中选择                                             |
| variables     | 定义全局变量                                                                                     |
| stage         | 选择全局定义的阶段，只能选其中一个                                                               |
| before_script | 在脚本执行前执行的指令                                                                           |
| after_script  | 在脚本执行后执行的指令                                                                           |
| script        | 由 runner 执行的 shell 脚本（必填项）                                                            |
| retry         | 发生故障时自动重试作业的时间和次数（0，1,2）                                                     |
| image         | 指定基础运行环境的 docker 镜像，如 java，python，maven 等                                        |
| tags          | 指定流水线使用哪个 runner 去运行，只能定义到一个具体的项目，tags 的取值范围是该项目可见的 runner |
| only          | 限定某些分支或者某些 tag                                                                         |
| except        | 排除某些分支和某些 tag                                                                           |
| services      | 使用 Docker services（服务）镜像                                                                 |
| when          | 什么时候运行作业                                                                                 |
| environment   | 部署的环境名称                                                                                   |
| cache         | 指定需要在 job 之间缓存的文件或目录                                                              |
| artifacts     | 归档文件列表，指定成功后应附加到 job 的文件和目录的列表                                          |
| dependencies  | 当前作业依赖的其他作业，你可以使用依赖作业的归档文件                                             |
| coverage      | 作业的代码覆盖率                                                                                 |
| parallel      | 指定并行运行的作业实例                                                                           |
| trigger       | 定义下游流水线的触发器                                                                           |
| include       | 作业加载其他 YAML 文件                                                                           |
| pages         | 上传 GitLab Pages 的结果                                                                         |
| allow_failure | 允许作业失败，失败的作业不影响提交的状态                                                         |

## 2. 对部分字段的详细注解

### 2.1 only / except

使用 only / except 关键字来控制何时创建作业

- 用 only 定义作业何时运行
- 用 except 定义作业何时不运行（除此之外都运行）

#### 2.1.1 refs

可选参数：

- 分支名称，如 master、main
- 匹配分支名称的正则表达式/^feature-.\*/
- 一下关键字
  | 属性           | 描述                                                                     |
  |----------------|--------------------------------------------------------------------------|
  | api            | 对于由管道 API 触发的管道。                                              |
  | branches       | 当管道的 Git 引用是一个分支时。                                          |
  | chat           | 对于使用 GitLab ChatOps 命令创建的管道。                                 |
  | external       | 当您使用 GitLab 以外的 CI 服务时。                                       |
  | merge_requests | 对于创建或更新合并请求时创建的管道。启用合并请求管道、合并结果管道和合并 |
  | pipelines      | 对于使用带有, 或关键字的 API 创建的多项目管道。CI_JOB_TOKENtrigger       |
  | pushes         | 对于由事件触发的管道 git push，包括分支和标签。                          |
  | tags           | 当管道的 Git 引用是 tag 时。                                             |
  | triggers       | 对于使用触发器令牌创建的管道。                                           |

例如

```yaml
job1:
  script: echo
  only:
    - main
    - /^issue-.*$/
    - merge_requests

job2:
  script: echo
  except:
    - main
    - /^stable-branch.*$/
```

默认情况如下，即未指定 only 和 except 时

```yaml
job1:
  script: echo "test"

# 此处 job2 和 job1 等价
job2:
  script: echo "test"
  only:
    - branches
    - tags
```

#### 2.1.2 variables

示例

```yaml
testCode:
  stage: test
  script:
    - echo "只有在type变量为running时才会运行流水线"
  only:
    variables:
      - $TYPE == 'running'
```

#### 2.1.3 changes

当 Git push 修改文件时，使用 changes 关键字 来判断是否修改了指定目录/文件下的内容。以此来判断是否新建任务。
常用参数：

- 文件路径
- 某个目录的通配符，例如 `path/to/directory/*` ，或一个目录及其所有子目录，例如 `path/to/directory/**/*`
- 具有相同扩展名或多个扩展名的所有文件的通配符 glob 路径，例如 `*.md` 或 `path/to/directory/*.{rb,py,sh}` .
- 根目录或所有目录中文件的通配符路径，用双引号引起来。例如 `".json"` 或 `"**/.json"` 。

示例
```yaml
testCode:
  stage: test
  script: 
    - echo "只有在分支修改了src文件夹之后才会执行流水线"
  only:
    refs:
      - branches
    changes:
      - src/*
```

#### 2.1.4 when

可选参数：
+ on_success（默认）：当早期阶段的所有任务都成功运行或者有配置 allow_failure: true.时运行当前任务
+ manual：在Gitlab UI中手动触发任务
+ always：无论早期阶段的作业状态如何，都运行作业。也可用于 workflow:rules.
+ on_failure：仅当至少一个早期阶段的作业失败时才运行该作业
+ delayed：将作业的执行延迟 指定的持续时间
+ never: 不要运行作业。只能在 rules 节或中使用 workflow: rules

## 3. 样例

```yaml
variables:
  COVERAGE_WEBHOOK_URL: $COVERAGE_WEBHOOK_URI?branch=$CI_COMMIT_REF_NAME&gitlabPipelineId=$CI_PIPELINE_ID&gitlabProjectId=$CI_PROJECT_ID
  IMAGE_WEBHOOK_URL: $CI_SERVICE_URL/webhook/gitlabProjects/$CI_PROJECT_ID/pipelines/$CI_PIPELINE_ID/images
  imageTag: $CI_PROJECT_NAME-$DEPLOY_TIME_TAG-$CI_PIPELINE_ID
  DOCKER_TLS_CERTDIR: ""

stages:
  - code_check
  - push_images
  - deploy

# 标题的code_check是job命名，和stage无关，可以为其他值，但是需要在stage字段指定stage
code_check:
  stage: code_check
  image: 10.19.64.203:8080/ums/maven:3.6.0-jdk-8
  # 指定该runner去执行当前阶段
  tags:
    - devops
  script:
    - mvn clean package -DskipTests
  artifacts:
    expire_in: 3 hrs
    paths:
      - ./ib-provider/target/*.jar

push_images:
  stage: push_images
  image: 10.19.64.203:8080/library/docker:19.03.14
  tags:
    - devops
  services:
    - 10.19.64.203:8080/library/docker:19.03.14-dind
  before_script:
    - mkdir -p $HOME/.docker
    - echo $DOCKER_AUTH_CONFIG > $HOME/.docker/config.json
    - docker info
  script:
    - docker build -t ${CI_REGISTRY_IMAGE_DIR}:${imageTag} .
    - docker push ${CI_REGISTRY_IMAGE_DIR}:${imageTag}
    - wget --post-data "imageTag=$imageTag" $IMAGE_WEBHOOK_URL

deploy:
  stage: deploy
  image: harbor.uniin.cn/devops-ci/mvn:1.0.1
  tags:
    - devops
  script:
    - echo $imageTag
    - wget --no-check-certificate --header="Authorization:$AUTH_TOKEN" --post-data="pipelineId=$CI_PIPELINE_ID&ref=$CI_COMMIT_REF_NAME&imageName=$imageName&tag=$imageTag&deployVersion=$deployVersion&description=$description&deployParams=$deployParams&envName=$envName&deployType=$deployType" $DEPLOY_WEBHOOK_URL
  only:
    variables:
      - $IS_DEPLOY == "1"
  dependencies:
    - push_images
```
