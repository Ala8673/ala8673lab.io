---
title: c++各类构造赋值函数
date: 2024-01-11 09:00:30
tags:
    - c/c++
    - 构造函数
    - 赋值函数
categories:
    - c/c++
    - 基础知识
---

## 1. 默认构造函数

```c++
class A{
public:
    A(){} // 默认构造函数
    // A() = default; // 默认构造函数
};
```

## 2. 拷贝构造函数

调用时机：
+ 用一个对象初始化另一个对象 `A a; A b(a);`
+ 以 **值传递** 的方式给函数参数传值 `void func(A a){}`
+ 以 **值传递** 的方式从函数返回对象 `A func(){return A();}`

<font style="color:red">
Attention:

返回局部变量的引用会引发错误，因为局部变量在函数结束后会被释放，引用就会变成悬空引用。

</font>

```c++
class A{
public:
    // 如果希望进行深拷贝，需要自己实现拷贝构造函数，处理指针的拷贝
    A(const A& a){} // 拷贝构造函数
};
```

## 3. 移动构造函数

调用时机：
+ 以 **右值引用** 的方式给函数参数传值 `void func(A&& a){}`
+ 以 **右值引用** 的方式从函数返回对象 `A func(){return A();}`
+ 用一个对象初始化另一个对象 `A a; A b(std::move(a));`

作用：
+ 用于将一个对象的资源转移给另一个对象，避免不必要的拷贝操作，提高效率
+ 用于实现移动语义，将资源所有权从一个对象转移到另一个对象，而不是拷贝 `A a; A b(std::move(a));`


<font style="color:red">
Attention:

移动构造需保证对象的资源所有权转移后，原对象的资源指针为空，否则会导致资源被释放两次。

</font>

```c++
class A{
public:
    A(A&& a){} // 移动构造函数
};
```

## 4. 拷贝赋值函数

调用时机：
+ 用一个对象初始化另一个对象 `A a; A b = a;`
+ 以 **值传递** 的方式给函数参数传值 `void func(A a){}`
+ 以 **值传递** 的方式从函数返回对象 `A func(){return A();}`

```c++
class A{
public:
    A& operator=(const A& a){ return *this; } // 拷贝赋值函数
};
```

## 5. 移动赋值函数

调用时机：
+ 以 **右值引用** 的方式给函数参数传值 `void func(A&& a){}`
+ 以 **右值引用** 的方式从函数返回对象 `A func(){return A();}`
+ 用一个对象初始化另一个对象 `A a; A b = std::move(a);`

```c++
class A{
public:
    A& operator=(A&& a){ return *this; } // 移动赋值函数
};
```

## 6. fully example

```c++  

#include <iostream>

using namespace std;

class A{
private:
    int* data;
public:
    A():data(new int(0)){}
    A(int d):data(new int(d)){}
    // 拷贝构造函数
    A(const A& a):data(new int(*a.data)){}
    // 移动构造函数
    A(A&& a):data(a.data){a.data = nullptr;}
    A& operator=(const A& a){
        if(this != &a){
            delete data;
            data = new int(*a.data);
        }
        return *this;
    }
    A& operator=(A&& a){
        if(this != &a){
            delete data;
            data = a.data;
            a.data = nullptr;
        }
        return *this;
    }
    ~A(){delete data;}
    int get_data(){return *data;}
};

void print(A &a){
    cout << "pointer: " << &a << endl;
}

int main(){
    A a(1);                         // 调用构造函数
    A b(a);                         // 调用拷贝构造函数
    A c(std::move(a));              // 调用移动构造函数
    A d = std::move(b);             // 调用移动构造函数

    A c = a;

    return 0;
}
```
