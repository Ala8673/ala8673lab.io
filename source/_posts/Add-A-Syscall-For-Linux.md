---
title: Add-A-Syscall-For-Linux
date: 2024-07-11 10:45:50
tags:
- linux
- kernel
- syscall
categories:
  - linux
  - kernel
---



# 环境介绍

操作系统：Fedora Server 40

```shell
NAME="Fedora Linux"
VERSION="40 (Server Edition)"
ID=fedora
VERSION_ID=40
VERSION_CODENAME=""
PLATFORM_ID="platform:f40"
PRETTY_NAME="Fedora Linux 40 (Server Edition)"
ANSI_COLOR="0;38;2;60;110;180"
LOGO=fedora-logo-icon
CPE_NAME="cpe:/o:fedoraproject:fedora:40"
HOME_URL="https://fedoraproject.org/"
DOCUMENTATION_URL="https://docs.fedoraproject.org/en-US/fedora/f40/system-administrators-guide/"
SUPPORT_URL="https://ask.fedoraproject.org/"
BUG_REPORT_URL="https://bugzilla.redhat.com/"
REDHAT_BUGZILLA_PRODUCT="Fedora"
REDHAT_BUGZILLA_PRODUCT_VERSION=40
REDHAT_SUPPORT_PRODUCT="Fedora"
REDHAT_SUPPORT_PRODUCT_VERSION=40
SUPPORT_END=2025-05-13
VARIANT="Server Edition"
VARIANT_ID=server
```

Kernel Version: 6.6.32



# 内核编译与运行

参见 [Build-Your-Own-Linux](https://ala8673-gitlab-io-ala8673-d4a5dc7bf9bbe70c03a4a25f2518f13d98786.gitlab.io/2024/07/04/Build-Your-Own-Linux/)



# 添加系统调用

+ ```arch/x86/syscalls/syscall_64.tbl```

​		仿照示例编写即可

```assembly
		454 common  mywrite         sys_mywrite
```

+ ```include/linux/syscalls.h```

```c
asmlinkage long sys_mywrite(const char __user* str, int count);
```

+ ```kernel/sys.c```

```c
SYSCALL_DEFINE2(mywrite, const char __user*, str, int, count)
{
	int ret;
	char buffer[256];
	if (count >= 256) {
		return -1;
	}
	ret = copy_from_user(&buffer, str, count);
	printk("app send %s to the kernel!\n", buffer);
	printk("%d\n", ret);
	return ret;
}
```

这里需要注意 ```SYSCALL_DEFINE2```，定义的系统调用需要使用宏去生成函数签名，改宏可以帮助实现针对不同架构下的差异。

同时，需要注意，**每一个token之间用逗号隔开**



# 编写程序调用

因为自己添加的系统调用也是x86架构下的，所以可以直接用宿主机的gcc编译，然后将编译后的程序拷贝到自己准备的文件系统中。

```c
#include <linux/kernel.h>
#include <stdio.h>
#include <string.h>
#include <sys/syscall.h>
#include <unistd.h>

int main() {
  char *buffer = "hello, linux kernel!";
  int ret;
  // 此处通过系统调用号调用
  ret = syscall(454, buffer, strlen(buffer) + 1);
  printf("syscall ret: %d\n", ret);
  return 0;
}
```

