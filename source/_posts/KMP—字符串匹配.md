---
title: KMP—字符串匹配
date: 2024-01-31 18:41:49
tags:
    - c/c++
    - 算法
    - KMP
    - 字符串匹配
categories:
    - 算法
---

# KMP—字符串匹配


## 思路

pat 表示模式串，txt 表示文本串

根据 pat 构建一个状态机，状态机的每个状态都是一个数组，数组的下标表示当前状态，数组的值表示当前状态下，遇到 txt 的某个字符应该跳转到的状态

## next 数组构建算法

```c++
// next 数组：
void build() {
    next = vector<int>(pat.length());
    for (int i = 1, j = 0; i < pat.length(); i++) {
        while (j > 0 && pat[i] != pat[j]) {
            // important: j - 1
            j = next[j - 1];
        }
        if (pat[i] == pat[j]) {
            j++;
        }
        next[i] = j;
    }
}
```

## next 数组构建思路

假设 `pat = "ababaca"`, i = 0

实际就是看 `pat` 的前缀和后缀的最长公共元素的长度，此处的前缀和后缀不包括整个字符串。

当 `i = 0` 时，字符串总体为 `"a"`， `pat` 的前缀和后缀都为空，所以 `next[0] = 0`

当 `i = 1` 时，字符串总体为 `"ab"`， `pat` 的前缀为 `"a"`，后缀为 `"b"`，没有公共元素，所以 `next[1] = 0`

当 `i = 2` 时，字符串总体为 `"aba"`， `pat` 的前缀为 `"a", "ab"`，后缀为 `"a", "ba"`，公共元素为 `"a"`，所以 `next[2] = 1`

当 `i = 3` 时，字符串总体为 `"abab"`， `pat` 的前缀为 `"a", "ab", "aba"`，后缀为 `"b", "ab", "bab"`，公共元素为 `"ab"`，所以 `next[3] = 2`

当 `i = 4` 时，字符串总体为 `"ababa"`， `pat` 的前缀为 `"a", "ab", "aba", "abab"`，后缀为 `"a", "ba", "aba", "baba"`，公共元素为 `"aba"`，所以 `next[4] = 3`

当 `i = 5` 时，字符串总体为 `"ababac"`， `pat` 的前缀为 `"a", "ab", "aba", "abab", "ababa"`，后缀为 `"c", "ac", "bac", "abac", "babac"`，没有公共元素，所以 `next[5] = 0`

当 `i = 6` 时，字符串总体为 `"ababaca"`， `pat` 的前缀为 `"a", "ab", "aba", "abab", "ababa", "ababac"`，后缀为 `"a", "ca", "aca", "baca", "abaca", "babaca"`，公共元素为 `"a"`，所以 `next[6] = 1`

## 匹配算法

```c++
int search(string& text) {
    for (int i = 0, j = 0; i < text.length(); i++) {
        while (j > 0 && text[i] != pat[j]) {
            j = next[j - 1];
        }
        if (text[i] == pat[j]) {
            j++;
        }
        if (j == pat.length()) {
            return i - pat.length() + 1;
        }
    }
    return -1;
}
```
