---
title: Build-Your-Own-Linux
date: 2024-07-04 22:55:27
tags:
  - linux
categories:
  - linux
---





# 环境介绍

操作系统：Fedora Server 40

```shell
NAME="Fedora Linux"
VERSION="40 (Server Edition)"
ID=fedora
VERSION_ID=40
VERSION_CODENAME=""
PLATFORM_ID="platform:f40"
PRETTY_NAME="Fedora Linux 40 (Server Edition)"
ANSI_COLOR="0;38;2;60;110;180"
LOGO=fedora-logo-icon
CPE_NAME="cpe:/o:fedoraproject:fedora:40"
HOME_URL="https://fedoraproject.org/"
DOCUMENTATION_URL="https://docs.fedoraproject.org/en-US/fedora/f40/system-administrators-guide/"
SUPPORT_URL="https://ask.fedoraproject.org/"
BUG_REPORT_URL="https://bugzilla.redhat.com/"
REDHAT_BUGZILLA_PRODUCT="Fedora"
REDHAT_BUGZILLA_PRODUCT_VERSION=40
REDHAT_SUPPORT_PRODUCT="Fedora"
REDHAT_SUPPORT_PRODUCT_VERSION=40
SUPPORT_END=2025-05-13
VARIANT="Server Edition"
VARIANT_ID=server
```

Kernel Version: 6.6.32

Buildroot Version: 2024.02.03 

*buildroot 需要建议安装在非root用户下，记得修改镜像源*



# 主要流程

1. 下载编译内核
2. 使用buildroot构建根文件系统
3. 使用qemu启动编译好的内核



## 1 下载编译内核

### 1.1 下载内核

 https://www.kernel.org/



### 1.2 编译内核

``` shell
# make x86_64_defconfig

make menuconfig
```

此处需要配置内核

```shell
General setup  --->

       ----> [*] Initial RAM filesystem and RAM disk (initramfs/initrd) support

    Device Drivers  --->

       [*] Block devices  --->

               <*>   RAM block device support
```

之后执行

```
make -jn
```

编译成功的内核位于```arch/x86_64/boot/bzImage```, ```arch/x86/boot/bzImage``` （前者通过软连接指向后者）



## 2 使用buildroot构建根文件系统

```shell
make menuconfig
```

根据自己需要进行一些配置

```shell
Target options  --->
	Target Architecture (x86_64)  --->
	Target Architecture Variant (x86-64)  --->
	Target Binary Format (ELF)  --->
System configuration  --->
	Init system (systemd)  --->
	Enable root login with password
Filesystem images  --->
	ext2/3/4 root filesystem
		ext2/3/4 variant (ext4)  --->
Bootloaders  --->
	grub2
	x86-64-efi
```



```shell
make -jn
```

之后可以在 ```output/images/```看到 ```rootfs.ext2  rootfs.ext4  rootfs.img.gz  rootfs.tar```



## 3  使用qemu启动编译好的内核

```shell
qemu-system-x86_64 
	-kernel /root/linux-6.6.32/arch/x86_64/boot/bzImage 
	-drive file=/home/zsx/buildroot-2024.02.3/output/images/rootfs.ext4,format=raw 
	-append "root=/dev/sda console=ttyS0" 
	-nographic
```











