---
title: lcp10-二叉树任务调度
date: 2025-03-03 14:49:42
tags:
  - c/c++
  - leetcode
  - 二叉树
  - 算法
  - 自底向上
categories:
  - c/c++
  - leetcode
---

# 二叉树任务调度

## 题目描述

<p>任务调度优化是计算机性能优化的关键任务之一。在任务众多时，不同的调度策略可能会得到不同的总体执行时间，因此寻求一个最优的调度方案是非常有必要的。</p>

<p>通常任务之间是存在依赖关系的，即对于某个任务，你需要先<strong>完成</strong>他的前导任务（如果非空），才能开始执行该任务。<strong>我们保证任务的依赖关系是一棵二叉树，</strong>其中 <code>root</code> 为根任务，<code>root.left</code> 和 <code>root.right</code> 为他的两个前导任务（可能为空），<code>root.val</code> 为其自身的执行时间。</p>

<p>在一个 CPU 核执行某个任务时，我们可以在任何时刻暂停当前任务的执行，并保留当前执行进度。在下次继续执行该任务时，会从之前停留的进度开始继续执行。暂停的时间可以不是整数。</p>

<p>现在，系统有<strong>两个</strong> CPU 核，即我们可以同时执行两个任务，但是同一个任务不能同时在两个核上执行。给定这颗任务树，请求出所有任务执行完毕的最小时间。</p>

<p><strong>示例 1：</strong></p>

<blockquote> 
 <p><img alt="image.png" src="https://pic.leetcode-cn.com/3522fbf8ce4ebb20b79019124eb9870109fdfe97fe9da99f6c20c07ceb1c60b3-image.png" /></p> 
</blockquote>

<p>输入：root = [47, 74, 31]</p>

<p>输出：121</p>

<p>解释：根节点的左右节点可以并行执行31分钟，剩下的43+47分钟只能串行执行，因此总体执行时间是121分钟。</p>

<p><strong>示例 2：</strong></p>

<blockquote> 
 <p><img alt="image.png" src="https://pic.leetcode-cn.com/13accf172ee4a660d241e25901595d55b759380b090890a17e6e7bd51a143e3f-image.png" /></p> 
</blockquote>

<p>输入：root = [15, 21, null, 24, null, 27, 26]</p>

<p>输出：87</p>

<p><strong>示例 3：</strong></p>

<blockquote> 
 <p><img alt="image.png" src="https://pic.leetcode-cn.com/bef743a12591aafb9047dd95d335b8083dfa66e8fdedc63f50fd406b4a9d163a-image.png" /></p> 
</blockquote>

<p>输入：root = [1,3,2,null,null,4,4]</p>

<p>输出：7.5</p>

<p><strong>限制：</strong></p>

<ul> 
 <li><code>1 &lt;= 节点数量 &lt;= 1000</code></li> 
 <li><code>1 &lt;= 单节点执行时间 &lt;= 1000</code></li> 
</ul>

<div><div>Related Topics</div><div><li>树</li><li>深度优先搜索</li><li>动态规划</li><li>二叉树</li></div></div><br><div><li>👍 78</li><li>👎 0</li></div>


## 题解

```c++
class Solution {
public:
    double minimalExecTime(TreeNode* root) {
        function<pair<double, double>(TreeNode*)> dfs = [&](TreeNode* root) -> pair<double, double> {
            if(!root) return {0.0, 0.0};
            auto [S1, T1] = dfs(root->left);
            auto [S2, T2] = dfs(root->right);
            return {S1 + S2 + root->val, max(max(T1, T2), (S1 + S2) / 2) + root->val};
        };
        return dfs(root).second;
    }
};
```

## 思路

首先给出结论，再证明。

### 结论：

设二叉树的一个节点中，
- 左子树的所有任务的时间和为 $S_1$，进行最优并行化后执行时间为 $T_1$；
- 右子树的所有任务的时间和为 $S_2$，进行最优并行化后执行时间为 $T_2$；

那么限制左、右子树的任务执行总时间的因素只有两个：
- 两侧任务时间和的平均值（也就是充分并行化的执行时间）$\displaystyle{\frac{S_1 + S_2}{2}}$；
- 任意一侧的充分并行化后的执行时间 $\max(T_1, T_2)$，因为合并后的总时间必然不比仅执行一侧的时间短。

因此合并执行时间 $T = \displaystyle{\max\left(\frac{S_1 + S_2}{2},\max(T_1, T_2) \right)}$。

### 证明：

不妨设 $S_1 \ge S_2$。现在考虑左、右侧子树全部执行完所需要的时间。假设我们让 CPU1 执行左侧子树的任务，CPU2 执行右侧子树的任务，那么总时间就是 $\max(S_1, S_2) = S_1$。

![image.png](https://pic.leetcode.cn/1676080751-hPaQLy-image.png)

但是这样产生了一段串行执行时间 $S_1 - S_2$。现在考虑将左子树的任务进行 **一定程度的** 并行化，把并行化的任务时间扔到 CPU2 上去执行。这将导致 CPU1 上的执行时间缩短，CPU2 上执行时间延长。

- 如果 $\displaystyle{T_1 \le \frac{S_1 + S_2}{2}}$，那么我们可以将 CPU1 上的时间减少到 $\displaystyle{\frac{S_1 + S_2}{2}}$，此时 CPU2 上的时间也将相应增大到 $\displaystyle{\frac{S_1 + S_2}{2}}$，此时达到了充分的并行化，最优执行时间就是 $\displaystyle{\frac{S_1 + S_2}{2}}$。

![image.png](https://pic.leetcode.cn/1676081079-eYtlbX-image.png)

- 如果 $\displaystyle{T_1 > \frac{S_1 + S_2}{2}}$，那么 CPU1 上的执行时间只能减小到 $T_1$，此时 CPU1 上的执行时间仍然大于 CPU2 上的执行时间，因此最优执行时间 = CPU1 上的执行时间 = $T_1$。

![image.png](https://pic.leetcode.cn/1676081258-mfwoRb-image.png)

综上所述，总执行时间 $T = \displaystyle{\max\left(\frac{S_1 + S_2}{2}, T_1\right)}$。注意到 $\displaystyle{T_2 \le S_2 \le \frac{S_1 + S_2}{2}}$（因为 $S_1 > S_2$），因此式子写成 $T = \displaystyle{\max\left(\frac{S_1 + S_2}{2},\max(T_1, T_2) \right)}$ 也是成立的。

### 用这个思路解释示例 3 为什么是 7.5

![image.png](https://pic.leetcode.cn/1676984780-gnHUJI-image.png)

### 代码

时间、空间复杂度：$O(n)$。

* c++

```c++
class Solution {
public:
    double minimalExecTime(TreeNode* root) {
        function<pair<double, double>(TreeNode*)> dfs = [&](TreeNode* root) -> pair<double, double> {
            if(!root) return {0.0, 0.0};
            auto [S1, T1] = dfs(root->left);
            auto [S2, T2] = dfs(root->right);
            return {S1 + S2 + root->val, max(max(T1, T2), (S1 + S2) / 2) + root->val};
        };
        return dfs(root).second;
    }
};
```


