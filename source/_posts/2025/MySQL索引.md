---
title: MySQL索引
date: 2025-03-10 20:14:00
tags:
  - 数据库
  - MySQL
  - 索引
categories:
  - 数据库
  - MySQL
  - 索引
---

# MySQL索引

## 1. 索引分类

### 1.1 按数据结构

- B+树索引 {% post_link "BX树" "（B+树相关点）" %}

适合范围查询和精确查询，支持有序数据的快速查找、范围查询，innodb 和 MyISAM 存储引擎的默认索引类型

- Hash索引

适合等值查询，不支持范围查询，只能进行等值查询，只能用于查询，不能用于排序，常用语 Memory 存储引擎

- Full-text全文索引

大多使用倒排索引实现，用于全文搜索，将全文分词，然后建立倒排索引，支持全文搜索，不支持范围查询

- R-Tree索引

GIS地理数据存储引擎，用于空间数据存储，支持空间数据的快速查找

### 1.2 innoDB常见索引

- 聚簇索引

索引的叶子节点存储的是数据行的实际数据，innodb 表的主键索引就是聚簇索引

- 非聚簇索引

非主键索引的叶子节点存储的是 **索引字段** 和 **主键字段** 的值，innodb 表的非主键索引就是非聚簇索引

### 1.3 按索引性质

- 普通索引
- 主键索引

每一行数据都有一个唯一的主键，主键索引是一种特殊的唯一索引，不允许有空值

- 联合索引

多个字段组合在一起建立的索引，可以提高多字段查询的效率

- 唯一索引

保证索引列的值是唯一的，但允许有空值

- 全文索引

对长文本字段进行关键字搜索，支持自然语言搜索、自然语言处理、模糊匹配等。

- 空间索引

## 2. 创建索引

### 2.1 主键索引

```sql
CREATE TABLE `t_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
```

### 2.2 普通索引

```sql
CREATE TABLE `t_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  KEY `idx_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

create index idx_name on t_user(name);
```

### 2.3 唯一索引

```sql
CREATE TABLE `t_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  UNIQUE KEY `idx_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

create unique index idx_name on t_user(name);
```

### 2.4 联合索引

```sql
CREATE TABLE `t_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  KEY `idx_name_age` (`name`, `age`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

create index idx_name_age on t_user(name, age);
```

### 2.5 全文索引

```sql
CREATE TABLE `t_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  FULLTEXT KEY `idx_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

create fulltext index idx_name on t_user(name);
```

### 2.6 空间索引

```sql
CREATE TABLE `t_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  SPATIAL KEY `idx_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

create spatial index idx_name on t_user(name);
```

### 2.6 哈希索引

```sql
CREATE TABLE `t_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  KEY `idx_name` (`name`) USING HASH
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

create index idx_name on t_user(name) using hash;
```


