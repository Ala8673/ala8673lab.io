---
title: stl-list
date: 2025-03-04 14:34:16
tags:
    - c/c++
    - stl
    - 侯捷
    - list
category:
    - c/c++
    - stl
mermaid: true
---

# stl-list

双向链表

## 类关系

```mermaid
classDiagram
    class list["list < T, Alloc >"] {
        typedef __list_imp<_Tp, _Alloc> base;
        typedef typename base::__node_pointer __node_pointer;
        typedef typename base::__node_base __node_base;
    }

    class list_impl["__list_impl < T, Alloc >"] {
        typedef _Tp value_type;
        typedef __list_iterator<value_type, __void_pointer> iterator;
        typedef __list_node_base<value_type, __void_pointer> __node_base;
        typedef __list_node<value_type, __void_pointer> __node_type;
        ===============================        
        __node_base __end_;
        __compressed_pair<size_type, __node_allocator> __size_alloc_;
    }
    
    list_impl --|> list : uses (private inheritance)
    
    class list_node["__list_node"] {
        union<_Tp> __value_;
    }
    
    class list_node_pointer_traits["__list_node_pointer_traits"] {
        typedef __rebind_pointer_t<_VoidPtr, __list_node<_Tp, _VoidPtr> > __node_pointer;
        typedef __rebind_pointer_t<_VoidPtr, __list_node_base<_Tp, _VoidPtr> > __base_pointer;
        typedef __conditional_t<is_pointer<_VoidPtr>::value, __base_pointer, __node_pointer> __link_pointer;
    }

    class list_node_base["__list_node_base"] {
        typedef __list_node_pointer_traits<_Tp, _VoidPtr> _NodeTraits;
        typedef typename _NodeTraits::__node_pointer __node_pointer;
        typedef typename _NodeTraits::__base_pointer __base_pointer;
        typedef typename _NodeTraits::__link_pointer __link_pointer;
        =======================================
        __node_base* __next_;
        __node_base* __prev_;
    }
    
    list_node_pointer_traits --> list_node_base
    list_node_base --|> list_node : uses (public inheritance)
    list_node --> list_impl
```

## sort

整体思路：归并排序

针对小链表做特殊处理

找出连续段，将一整段进行归并

```cpp
template <class _Tp, class _Alloc>
template <class _Comp>
typename list<_Tp, _Alloc>::iterator
list<_Tp, _Alloc>::__sort(iterator __f1, iterator __e2, size_type __n, _Comp& __comp) {

  // 小链表排序
  switch (__n) {
  case 0:
  case 1:
    return __f1;
  case 2:
    if (__comp(*--__e2, *__f1)) {
      __link_pointer __f = __e2.__ptr_;
      base::__unlink_nodes(__f, __f);
      __link_nodes(__f1.__ptr_, __f, __f);
      return __e2;
    }
    return __f1;
  }
  
  // 取出一半元素
  size_type __n2 = __n / 2;
  iterator __e1  = std::next(__f1, __n2);
  // 进行分治，然后归并
  iterator __r = __f1 = __sort(__f1, __e1, __n2, __comp);
  iterator __f2 = __e1 = __sort(__e1, __e2, __n - __n2, __comp);
  
  // 开始归并，确定最终链表的头部
  if (__comp(*__f2, *__f1)) {
    iterator __m2 = std::next(__f2);
    for (; __m2 != __e2 && __comp(*__m2, *__f1); ++__m2)
      ;
    __link_pointer __f = __f2.__ptr_;
    __link_pointer __l = __m2.__ptr_->__prev_;
    __r                = __f2;
    __e1 = __f2 = __m2;
    base::__unlink_nodes(__f, __l);
    __m2 = std::next(__f1);
    __link_nodes(__f1.__ptr_, __f, __l);
    __f1 = __m2;
  } else
    ++__f1;

  while (__f1 != __e1 && __f2 != __e2) {
    if (__comp(*__f2, *__f1)) {
      iterator __m2 = std::next(__f2);
      
      // 找到连续段
      for (; __m2 != __e2 && __comp(*__m2, *__f1); ++__m2)
        ;
      __link_pointer __f = __f2.__ptr_;
      __link_pointer __l = __m2.__ptr_->__prev_;
      if (__e1 == __f2)
        __e1 = __m2;
      __f2 = __m2;
      base::__unlink_nodes(__f, __l);
      __m2 = std::next(__f1);
      __link_nodes(__f1.__ptr_, __f, __l);
      __f1 = __m2;
    } else
      ++__f1;
  }
  return __r;
}
```


