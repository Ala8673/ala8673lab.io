---
title: stl-string
date: 2025-03-06 16:53:25
tags:
  - c/c++
  - stl
  - string
categories:
  - c/c++
  - stl
---

# STL string

## string存储在栈内存还是堆内存

`std::string` 有三种存储方式：

+ **短字符串优化**：小字符串直接存储在 `std::string` 对象内部，不需要额外的内存分配。
+ **长字符串**：长字符串存储在堆内存中，`std::string` 对象内部存储指向堆内存的指针。
+ **COW**：Copy On Write，写时复制，当 `std::string` 对象被拷贝时，只有引用计数加一，实际数据不会被拷贝，只有在写操作时才会真正拷贝数据。

## 对于长短字符串的定义


```cpp
  struct __long {
    struct _LIBCPP_PACKED {
      size_type __is_long_ : 1;
      size_type __cap_ : sizeof(size_type) * CHAR_BIT - 1;
    };
    size_type __size_;
    pointer __data_;
  };

  enum { __min_cap = (sizeof(__long) - 1) / sizeof(value_type) > 2 ? (sizeof(__long) - 1) / sizeof(value_type) : 2 };

  struct __short {
    struct _LIBCPP_PACKED {
      unsigned char __is_long_ : 1;
      unsigned char __size_    : 7;
    };
    char __padding_[sizeof(value_type) - 1];
    value_type __data_[__min_cap];
  };
```