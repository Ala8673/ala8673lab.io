---
title: stl-deque-queue-stack
date: 2025-03-04 21:13:54
tags:
  - c/c++
  - stl
  - deque
  - queue
  - stack
categories:
  - c/c++
  - stl
---

# STL Deque

Deque 是双端队列，支持随机访问

底层实现是一个指针数组（ map，存储指向块的指针，是一个 split buffer ）和块（ block ，存储元素），参考 [deque实际访问方式](#operator)

```cpp


## block size

```cpp
template <class _ValueType, class _DiffType>
struct __deque_block_size {
  static const _DiffType value = sizeof(_ValueType) < 256 ? 4096 / sizeof(_ValueType) : 16;
};

template <class _Tp, class _Allocator /*= allocator<_Tp>*/>
class _LIBCPP_TEMPLATE_VIS deque {
public:
    static const difference_type __block_size;
    .......    
};


template <class _Tp, class _Alloc>
_LIBCPP_CONSTEXPR const typename allocator_traits<_Alloc>::difference_type deque<_Tp, _Alloc>::__block_size =
    __deque_block_size<value_type, difference_type>::value;
```

## operator[]

```cpp
template <class _Tp, class _Allocator>
inline typename deque<_Tp, _Allocator>::reference deque<_Tp, _Allocator>::operator[](size_type __i) _NOEXCEPT {
  _LIBCPP_ASSERT_VALID_ELEMENT_ACCESS(__i < size(), "deque::operator[] index out of bounds");
  size_type __p = __start_ + __i;
  // 按照块大小计算索引
  return *(*(__map_.begin() + __p / __block_size) + __p % __block_size);
}
```

## emplae_back

```cpp
template <class _Tp, class _Allocator>
template <class... _Args>
#  if _LIBCPP_STD_VER >= 17
typename deque<_Tp, _Allocator>::reference
#  else
void
#  endif
deque<_Tp, _Allocator>::emplace_back(_Args&&... __args) {
  allocator_type& __a = __alloc();
  // size_type __back_spare() { return __capacity() - (__start_ + size()); }
  if (__back_spare() == 0)
    // 
    __add_back_capacity();
  // __back_spare() >= 1
  // 内存跟踪（跳过）
  __annotate_increase_back(1);
  // 构造元素
  __alloc_traits::construct(__a, std::addressof(*end()), std::forward<_Args>(__args)...);
  ++__size();
#  if _LIBCPP_STD_VER >= 17
  return *--end();
#  endif
}
```

`__add_back_capacity` 用于增加后段的容量

分配流程：

+ 如果前段的空间大于一个块的大小，将前段的数据向后移动一个块的大小 
+ 如果 `__map` 还有空间，直接在后段分配 
+ 如果 `__map` 没有空间，需要重新分配 `__map`，设置前段空闲为 `__map.size()`，再 `push_front` ，相当于将所有空间预留给后段


```cpp
// Create back capacity for one block of elements.
// Strong guarantee.  Either do it or don't touch anything.
template <class _Tp, class _Allocator>
void deque<_Tp, _Allocator>::__add_back_capacity() {
  allocator_type& __a = __alloc();
  // 如果前段的空间大于一个块的大小，将前段的数据向后移动一个块的大小
  if (__front_spare() >= __block_size) {
    __start_ -= __block_size;
    pointer __pt = __map_.front();
    __map_.pop_front();
    __map_.push_back(__pt);
  }
  // Else if __nb <= __map_.capacity() - __map_.size() then we need to allocate __nb buffers
  else if (__map_.size() < __map_.capacity()) { // we can put the new buffer into the map, but don't shift things around
    // until it is allocated.  If we throw, we don't need to fix
    // anything up (any added buffers are undetectible)
    // 如果后段空间不为0，直接在后段分配
    if (__map_.__back_spare() != 0)
      __map_.push_back(__alloc_traits::allocate(__a, __block_size));
    // 如果后段空间为0（前段空间小于一个block），向前段分配一个块，然后移动到后段
    else {
      __map_.push_front(__alloc_traits::allocate(__a, __block_size));
      // Done allocating, reorder capacity
      pointer __pt = __map_.front();
      __map_.pop_front();
      __map_.push_back(__pt);
    }
    __annotate_whole_block(__map_.size() - 1, __asan_poison);
  }
  // Else need to allocate 1 buffer, *and* we need to reallocate __map_.
  else {
      // 前段预留 __map.size() 的空间，此时 __map.size() == __map.capacity()
    __split_buffer<pointer, __pointer_allocator&> __buf(
        std::max<size_type>(2 * __map_.capacity(), 1), __map_.size(), __map_.__alloc());
    
    typedef __allocator_destructor<_Allocator> _Dp;
    unique_ptr<pointer, _Dp> __hold(__alloc_traits::allocate(__a, __block_size), _Dp(__a, __block_size));
    __buf.push_back(__hold.get());
    __hold.release();
    // 空间全预留给了后段
    for (__map_pointer __i = __map_.end(); __i != __map_.begin();)
      __buf.push_front(*--__i);
    std::swap(__map_.__first_, __buf.__first_);
    std::swap(__map_.__begin_, __buf.__begin_);
    std::swap(__map_.__end_, __buf.__end_);
    std::swap(__map_.__end_cap(), __buf.__end_cap());
    __annotate_whole_block(__map_.size() - 1, __asan_poison);
  }
}
```

```cpp
template <class _Tp, class _Allocator /*= allocator<_Tp>*/>
class _LIBCPP_TEMPLATE_VIS deque {
public:
    // 存储元素类型的指针
    using __map = __split_buffer<pointer, __pointer_allocator>;
    __map __map_;
    
    // 每块的大小
    static const difference_type __block_size;
    size_type __start_;
    __compressed_pair<size_type, allocator_type> __size_;
    
};

```

# STL Queue

```cpp
// file include/__fwd/queue
template <class _Tp, class _Container = deque<_Tp> >
class _LIBCPP_TEMPLATE_VIS queue;

template <class _Tp, class _Container = vector<_Tp>, class _Compare = less<typename _Container::value_type> >
class _LIBCPP_TEMPLATE_VIS priority_queue;
```

# STL Stack

```cpp
// file include/__fwd/stack
template <class _Tp, class _Container = deque<_Tp> >
class _LIBCPP_TEMPLATE_VIS stack;
```
