---
title: stl-iterator
date: 2025-03-04 15:38:39
tags:
  - c/c++
  - stl
  - iterator
categories:
  - c/c++
  - stl
---

# STL 迭代器

## 介绍

迭代器是指针的泛化，用于访问 STL 容器的元素。它们提供了一种遍历容器元素的方法，而不暴露底层表示。

## 迭代器类型

1. **InputIterator**：只读访问序列。
2. **OutputIterator**：只写访问序列。
3. **Forward Iterator**：读写访问，只能向前移动。
4. **Bidirectional Iterator**：读写访问，可以向前和向后移动。
5. **Random Access Iterator**：读写访问，可以在常数时间内移动到任何元素。

## 迭代器特性

迭代器特性提供了一种提取迭代器类型信息的方法。`std::iterator_traits` 模板用于此目的。

## list iterator

### iterator 必须提供的类型

```cpp
  typedef bidirectional_iterator_tag iterator_category;
  typedef _Tp value_type;
  typedef value_type& reference;
  typedef __rebind_pointer_t<_VoidPtr, value_type> pointer;
  
  // typedef ptrdiff_t difference_type;
  typedef typename pointer_traits<pointer>::difference_type difference_type;
```

### 源码

```cpp

template <class _Tp, class _VoidPtr>
class _LIBCPP_TEMPLATE_VIS __list_iterator {
  typedef __list_node_pointer_traits<_Tp, _VoidPtr> _NodeTraits;
  typedef typename _NodeTraits::__link_pointer __link_pointer;

  __link_pointer __ptr_;

  _LIBCPP_HIDE_FROM_ABI explicit __list_iterator(__link_pointer __p) _NOEXCEPT : __ptr_(__p) {}

  template <class, class>
  friend class list;
  template <class, class>
  friend class __list_imp;
  template <class, class>
  friend class __list_const_iterator;

public:
  typedef bidirectional_iterator_tag iterator_category;
  typedef _Tp value_type;
  typedef value_type& reference;
  typedef __rebind_pointer_t<_VoidPtr, value_type> pointer;
  typedef typename pointer_traits<pointer>::difference_type difference_type;

  _LIBCPP_HIDE_FROM_ABI __list_iterator() _NOEXCEPT : __ptr_(nullptr) {}

  _LIBCPP_HIDE_FROM_ABI reference operator*() const { return __ptr_->__as_node()->__get_value(); }
  _LIBCPP_HIDE_FROM_ABI pointer operator->() const {
    return pointer_traits<pointer>::pointer_to(__ptr_->__as_node()->__get_value());
  }

  _LIBCPP_HIDE_FROM_ABI __list_iterator& operator++() {
    __ptr_ = __ptr_->__next_;
    return *this;
  }
  _LIBCPP_HIDE_FROM_ABI __list_iterator operator++(int) {
    __list_iterator __t(*this);
    ++(*this);
    return __t;
  }

  _LIBCPP_HIDE_FROM_ABI __list_iterator& operator--() {
    __ptr_ = __ptr_->__prev_;
    return *this;
  }
  _LIBCPP_HIDE_FROM_ABI __list_iterator operator--(int) {
    __list_iterator __t(*this);
    --(*this);
    return __t;
  }

  friend _LIBCPP_HIDE_FROM_ABI bool operator==(const __list_iterator& __x, const __list_iterator& __y) {
    return __x.__ptr_ == __y.__ptr_;
  }
  friend _LIBCPP_HIDE_FROM_ABI bool operator!=(const __list_iterator& __x, const __list_iterator& __y) {
    return !(__x == __y);
  }
};
```

```cpp
template <class _Tp>
struct _LIBCPP_TEMPLATE_VIS pointer_traits<_Tp*> {
  typedef _Tp* pointer;
  typedef _Tp element_type;
  typedef ptrdiff_t difference_type;

#ifndef _LIBCPP_CXX03_LANG
  template <class _Up>
  using rebind = _Up*;
#else
  template <class _Up>
  struct rebind {
    typedef _Up* other;
  };
#endif

private:
  struct __nat {};

public:
  _LIBCPP_HIDE_FROM_ABI _LIBCPP_CONSTEXPR_SINCE_CXX20 static pointer
  pointer_to(__conditional_t<is_void<element_type>::value, __nat, element_type>& __r) _NOEXCEPT {
    return std::addressof(__r);
  }
};
```