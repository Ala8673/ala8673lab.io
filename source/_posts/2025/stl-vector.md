---
title: stl-vector
date: 2025-03-04 16:19:47
tags:
  - c/c++
  - stl
  - vector
categories:
  - c/c++
  - stl
---

# STL vector

## 增长策略

libcxx: 两倍增长

```cpp
//  Precondition:  __new_size > capacity()
template <class _Tp, class _Allocator>
_LIBCPP_CONSTEXPR_SINCE_CXX20 inline _LIBCPP_HIDE_FROM_ABI typename vector<_Tp, _Allocator>::size_type
vector<_Tp, _Allocator>::__recommend(size_type __new_size) const {
  const size_type __ms = max_size();
  if (__new_size > __ms)
    this->__throw_length_error();
  const size_type __cap = capacity();
  if (__cap >= __ms / 2)
    return __ms;
  return std::max<size_type>(2 * __cap, __new_size);
}
```

MSVC-STL: 1.5倍增长

```cpp
_CONSTEXPR20 size_type _Calculate_growth(const size_type _Newsize) const {
  // given _Oldcapacity and _Newsize, calculate geometric growth
  const size_type _Oldcapacity = capacity();
  const auto _Max              = max_size();

  if (_Oldcapacity > _Max - _Oldcapacity / 2) {
      return _Max; // geometric growth would overflow
  }

  const size_type _Geometric = _Oldcapacity + _Oldcapacity / 2;

  if (_Geometric < _Newsize) {
      return _Newsize; // geometric growth would be insufficient
  }

  return _Geometric; // geometric growth is sufficient
}
```

# push_back

整体流程：
  + 先判断容量是否有剩余，有则直接在尾部构造对象，
  + 没有剩余容量则调用 `__push_back_slow_path` 进行扩容
    + 
`__construct_one_at_end` 最终会调用 `operator new` 进行对象构造，
根据参数确定是移动构造还是拷贝构造等等。

```cpp
// include/__memory/construct_at.h
template <class _Tp, class... _Args, class = decltype(::new(std::declval<void*>()) _Tp(std::declval<_Args>()...))>
_LIBCPP_HIDE_FROM_ABI _LIBCPP_CONSTEXPR_SINCE_CXX20 _Tp* __construct_at(_Tp* __location, _Args&&... __args) {
#if _LIBCPP_STD_VER >= 20
  return std::construct_at(__location, std::forward<_Args>(__args)...);
#else
  // 进行对象构造
  return _LIBCPP_ASSERT_NON_NULL(__location != nullptr, "null pointer given to construct_at"),
         ::new (std::__voidify(*__location)) _Tp(std::forward<_Args>(__args)...);
#endif
}
```

```cpp
// include/vector
template <class _Tp, class _Allocator>
_LIBCPP_CONSTEXPR_SINCE_CXX20 inline _LIBCPP_HIDE_FROM_ABI void
vector<_Tp, _Allocator>::push_back(const_reference __x) {
  pointer __end = this->__end_;
  // 容量足够
  if (__end < this->__end_cap()) {
    __construct_one_at_end(__x);
    ++__end;
  } else {
  // 容量不足
    __end = __push_back_slow_path(__x);
  }
  this->__end_ = __end;
}

template <class _Tp, class _Allocator>
_LIBCPP_CONSTEXPR_SINCE_CXX20 inline _LIBCPP_HIDE_FROM_ABI void vector<_Tp, _Allocator>::push_back(value_type&& __x) {
  pointer __end = this->__end_;
  if (__end < this->__end_cap()) {
    __construct_one_at_end(std::move(__x));
    ++__end;
  } else {
    __end = __push_back_slow_path(std::move(__x));
  }
  this->__end_ = __end;
}

template <class _Tp, class _Allocator>
template <class _Up>
_LIBCPP_CONSTEXPR_SINCE_CXX20 typename vector<_Tp, _Allocator>::pointer
vector<_Tp, _Allocator>::__push_back_slow_path(_Up&& __x) {
  // 获取分配器
  allocator_type& __a = this->__alloc();
  // 创建新空间
  __split_buffer<value_type, allocator_type&> __v(__recommend(size() + 1), size(), __a);
  // __v.push_back(std::forward<_Up>(__x));
  // 在新空间末尾构造对象
  __alloc_traits::construct(__a, std::__to_address(__v.__end_), std::forward<_Up>(__x));
  __v.__end_++;
  // 交换新旧空间
  __swap_out_circular_buffer(__v);
  return this->__end_;
}
```

## emploce_back

`emplace_back` 与 `push_back` 主要的区别是两者的参数。

+ `emplace_back` 支持可变参数，可以通过参数直接在容器内空间构造对象。
+ 如果传入的参数是一个对象，`emplace_back` 会调用移动构造函数，而 `push_back` 会调用拷贝构造函数。

```cpp
template <class _Tp, class _Allocator>
template <class... _Args>
_LIBCPP_CONSTEXPR_SINCE_CXX20 typename vector<_Tp, _Allocator>::pointer
vector<_Tp, _Allocator>::__emplace_back_slow_path(_Args&&... __args) {
  allocator_type& __a = this->__alloc();
  __split_buffer<value_type, allocator_type&> __v(__recommend(size() + 1), size(), __a);
  //    __v.emplace_back(std::forward<_Args>(__args)...);
  __alloc_traits::construct(__a, std::__to_address(__v.__end_), std::forward<_Args>(__args)...);
  __v.__end_++;
  __swap_out_circular_buffer(__v);
  return this->__end_;
}

template <class _Tp, class _Allocator>
template <class... _Args>
_LIBCPP_CONSTEXPR_SINCE_CXX20 inline
#if _LIBCPP_STD_VER >= 17
    typename vector<_Tp, _Allocator>::reference
#else
    void
#endif
    vector<_Tp, _Allocator>::emplace_back(_Args&&... __args) {
  pointer __end = this->__end_;
  if (__end < this->__end_cap()) {
    __construct_one_at_end(std::forward<_Args>(__args)...);
    ++__end;
  } else {
    __end = __emplace_back_slow_path(std::forward<_Args>(__args)...);
  }
  this->__end_ = __end;
#if _LIBCPP_STD_VER >= 17
  return *(__end - 1);
#endif
}
```
