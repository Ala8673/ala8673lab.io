---
title: stl-splite_buffer
date: 2025-03-04 20:57:35
tags:
  - c/c++
  - stl
  - splite_buffer
categories:
  - c/c++
  - stl
---

# STL splite_buffer

## 概述

`__split_buffer` 是一个辅助类，用于管理 `vector` 和 `deque` 的内存分配。
它是一个分裂缓冲区，可以在前后两端增长，而不必移动数据。
分为前段空闲去 `[__first_, __begin_)`，数据区 `[__begin_, __end_)` ，后段空闲区 `[__end_, __end_cap_.first())`。

```cpp
// __split_buffer allocates a contiguous chunk of memory and stores objects in the range [__begin_, __end_).
// It has uninitialized memory in the ranges  [__first_, __begin_) and [__end_, __end_cap_.first()). That allows
// it to grow both in the front and back without having to move the data.
```

## push_back

```cpp
template <class _Tp, class _Allocator>
_LIBCPP_CONSTEXPR_SINCE_CXX20 inline _LIBCPP_HIDE_FROM_ABI void
__split_buffer<_Tp, _Allocator>::push_back(const_reference __x) {
    // 后段空间不足
  if (__end_ == __end_cap()) {
    // 前段空间足够，将向前移动
    if (__begin_ > __first_) {
      difference_type __d = __begin_ - __first_;
      __d                 = (__d + 1) / 2;
      __end_              = std::move(__begin_, __end_, __begin_ - __d);
      __begin_ -= __d;
    } else {
      // 前段空间不足，重新分配
      // 尝试分配原来的两倍
      size_type __c = std::max<size_type>(2 * static_cast<size_t>(__end_cap() - __first_), 1);
      // 分配新的空间，并指明数据区起始为 capacity / 4 
      __split_buffer<value_type, __alloc_rr&> __t(__c, __c / 4, __alloc());
      // 将数据区的数据移动到新的空间
      __t.__construct_at_end(move_iterator<pointer>(__begin_), move_iterator<pointer>(__end_));
      std::swap(__first_, __t.__first_);
      std::swap(__begin_, __t.__begin_);
      std::swap(__end_, __t.__end_);
      std::swap(__end_cap(), __t.__end_cap());
    }
  }
  // 在数据区的末尾构造新的元素
  __alloc_traits::construct(__alloc(), std::__to_address(__end_), __x);
  ++__end_;
}
```

## push_front

整体思路同上

```cpp
template <class _Tp, class _Allocator>
_LIBCPP_CONSTEXPR_SINCE_CXX20 void __split_buffer<_Tp, _Allocator>::push_front(const_reference __x) {
  if (__begin_ == __first_) {
    if (__end_ < __end_cap()) {
      difference_type __d = __end_cap() - __end_;
      __d                 = (__d + 1) / 2;
      __begin_            = std::move_backward(__begin_, __end_, __end_ + __d);
      __end_ += __d;
    } else {
      size_type __c = std::max<size_type>(2 * static_cast<size_t>(__end_cap() - __first_), 1);
      __split_buffer<value_type, __alloc_rr&> __t(__c, (__c + 3) / 4, __alloc());
      __t.__construct_at_end(move_iterator<pointer>(__begin_), move_iterator<pointer>(__end_));
      std::swap(__first_, __t.__first_);
      std::swap(__begin_, __t.__begin_);
      std::swap(__end_, __t.__end_);
      std::swap(__end_cap(), __t.__end_cap());
    }
  }
  __alloc_traits::construct(__alloc(), std::__to_address(__begin_ - 1), __x);
  --__begin_;
}
```

