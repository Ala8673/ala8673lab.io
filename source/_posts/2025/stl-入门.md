---
title: stl-入门
date: 2025-03-03 09:37:18
tags:
    - c/c++
    - stl
    - 侯捷
category:
    - c/c++
    - stl
---

# stl-入门

## 标准库6大组件

+ 容器 (Container)
+ 算法 (Algorithm)
+ 迭代器 (Iterator)
+ 仿函数 (Function Object)
+ 适配器 (Adapter)
+ 内存分配器 (Allocator)


## example

```cpp
#include <iostream>
#include <vector>
#include <algorithm>
#include <functional>

int main() {
    int val[] = {1, 2, 3, 4, 5};
    // 通过添加模版参数，指定使用的内存分配器
    std::vector<int, allocator<int>> vec(val, val + 5);
    
    // count_if()  算法  统计满足条件的元素个数
    // not1()  适配器  适配器
    // bind2nd()  仿函数  绑定第二个参数
    // less<int>()  仿函数  二元谓词
    cout << count_if(vec.begin(), vec.end(), not1(bind2nd(less<int>(), 3))) <<endl;  

    return 0;
}
```
