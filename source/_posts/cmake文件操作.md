---
title: cmake文件操作
date: 2024-01-02 23:44:01
tags: 
    - cmake
    - 文件操作
categories:
    - 常用工具
---

## 文件操作
### 1. 概要

```cmake
Reading
  file(READ <filename> <out-var> [...])
  file(STRINGS <filename> <out-var> [...])
  file(<HASH> <filename> <out-var>)
  file(TIMESTAMP <filename> <out-var> [...])
  file(GET_RUNTIME_DEPENDENCIES [...])

Writing
  file({WRITE | APPEND} <filename> <content>...)
  file({TOUCH | TOUCH_NOCREATE} [<file>...])
  file(GENERATE OUTPUT <output-file> [...])
  file(CONFIGURE OUTPUT <output-file> CONTENT <content> [...])

Filesystem
  file({GLOB | GLOB_RECURSE} <out-var> [...] [<globbing-expr>...])
  file(RENAME <oldname> <newname>)
  file({REMOVE | REMOVE_RECURSE } [<files>...])
  file(MAKE_DIRECTORY [<dir>...])
  file({COPY | INSTALL} <file>... DESTINATION <dir> [...])
  file(SIZE <filename> <out-var>)
  file(READ_SYMLINK <linkname> <out-var>)
  file(CREATE_LINK <original> <linkname> [...])
  file(CHMOD <files>... <directories>... PERMISSIONS <permissions>... [...])
  file(CHMOD_RECURSE <files>... <directories>... PERMISSIONS <permissions>... [...])

Path Conversion
  file(REAL_PATH <path> <out-var> [BASE_DIRECTORY <dir>])
  file(RELATIVE_PATH <out-var> <directory> <file>)
  file({TO_CMAKE_PATH | TO_NATIVE_PATH} <path> <out-var>)

Transfer
  file(DOWNLOAD <url> [<file>] [...])
  file(UPLOAD <file> <url> [...])

Locking
  file(LOCK <path> [...])

Archiving
  file(ARCHIVE_CREATE OUTPUT <archive> PATHS <paths>... [...])
  file(ARCHIVE_EXTRACT INPUT <archive> [...])
```

### 读文件

```cmake
file(READ <filename> <variable> [OFFSET <offset>] [LIMIT <limit>] [HEX])
```
从 `<filename>` 文件中读取内容，并将其存储在 `<variable>` 变量中。`<offset>` 表示从该文件的偏移位置开始，`<limit>` 表示最多读取的字节数。`HEX` 将文件内容作为十六进制读取，输出字母 a 到 f 为小写。

### 读取文件作为 ASCII 字符串列表
```cmake
file(STRINGS <filename> <variable> [<options>...])
```
从 `<filename>` 文件解析 ASCII 字符串列表，并将其存储在 `<variable>` 中。忽略文件中的二进制数据，忽略回车符(\r, CR)。每行字符串为列表中一个元素。

`<options>` 选项包括：

LENGTH_MAXIMUM `<max-len>`: 只考虑不超过给定长度的字符串。
LENGTH_MINIMUM `<min-len>`: 只考虑不少于给定长度的字符串。
LIMIT_COUNT `<max-num>`: 限制要提取的不同字符串的数量。
LIMIT_INPUT `<max-in>`: 限制从文件中读取的输入字节数。
LIMIT_OUTPUT `<max-out>`: 限制 `<variable>` 中存储的总字节数。
NEWLINE_CONSUME: 将换行符(\n, LF)作为字符串内容的一部分，而不是终止于它们。
NO_HEX_CONVERSION: 除非给出此选项，否则读取时 Intel Hex 和 Motorola S-record 文件会自动转换为二进制文件。
REGEX `<regex>`: 只考虑匹配给定正则表达式的字符串，如 string(REGEX) 所述。如果匹配成功，包括该行。
ENCODING `<encoding-type>`: 考虑给定编码的字符串。目前支持的编码有：UTF-8、UTF-16LE、UTF-16BE、UTF-32LE、UTF-32BE。如果没有提供编码选项，并且文件具有字节顺序标记，则默认情况下，编码选项将遵循字节顺序标记。
例如，
```cmake
file(STRINGS "data.txt" VAR REGEX "Pride")
```
从文件 “data.txt” 文件解析为字符串列表，其中每一个元素为文件的一行, 并且只有包含字符串 "Pride" 的会被保留。
