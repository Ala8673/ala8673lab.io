---
title: 设计模式（c++）
date: 2024-01-12 18:39:45
tags:
- c/c++
- 设计模式
categories:
- c/c++
- 设计模式
---

<font style="color:red">
Attention:

父类析构函数用虚函数，子类析构函数也用虚函数，否则会造成内存泄漏。
</font>

```c++
#include <iostream>

using namespace std;

class Parent {
public:
  Parent() { cout << "Parent" << endl; }

  //   ~Parent() { cout << "~Parent" << endl; }
  virtual ~Parent() { cout << "~Parent" << endl; }
};

class Child : public Parent {
public:
  Child() { cout << "Child" << endl; }

  ~Child() { cout << "~Child" << endl; }
};

int main() {
  Parent *child = new Child();
  delete child;
  // output(not virtual destructor of parent):
  //   Parent
  //   Child
  //   ~Parent

  // output(virtual destructor of parent):
  //   Parent
  //   Child
  //   ~Child
  //   ~Parent

  return 0;
}

``` 

# 1. 面向对象设计原则 

<table>
<thead>
    <tr>
        <th>原则名称</th>
        <th>原则描述</th>
        <th>备注</th>
    </tr>
</thead>
<tbody>
    <tr>
        <td>开闭原则</td>
        <td>对扩展开放，对修改关闭。</td>
        <td></td>
    </tr>
    <tr>
        <td>依赖倒置原则</td>
        <td>高层模块（稳定）不应该依赖底层模块（变化），二者都应该依赖其抽象（稳定）；抽象（稳定）不应该依赖细节（变化），细节（变化）应该依赖抽象（稳定）。</td>
        <td></td>
    </tr>
    <tr>
        <td>单一职责原则</td>
        <td>一个类应该仅有一个引起它变化的原因。</td>
        <td>避免一个类过大，过于复杂，<mark>承担过多责任</mark></td>
    </tr>
    <tr>
        <td>Liskov替换原则</td>
        <td>子类必须能够替换掉它们的父类型。</td>
        <td>子类应该能够完美替代父类，否则就说明这个子类不应该继承自该父类。</td>
    </tr>
    <tr>
        <td>接口隔离原则</td>
        <td>不应该强迫客户程序依赖它们不用的方法。</td>
        <td>接口应该小而完备。</td>
    </tr>
    <tr>
        <td>组合优于继承</td>
        <td>类应该通过它们需要的行为，而不是它们的类型来相互关联。</td>
        <td>继承近似于白盒复用，组合通常为黑盒复用，继承相当于破坏了封装性。</td>
    </tr>
    <tr>
        <td>封装变化点</td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>针对接口编程，而不是针对实现编程</td>
        <td></td>
        <td></td>
    </tr>
</tbody>
</table>

# 2. 设计模式的分类

<table>
    <thead>
        <tr>
            <th>分类</th>
            <th>设计模式</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>组件协作</td>
            <td>Template Method、Strategy、Observer/Event</td>
        </tr>
        <tr>
            <td>单一职责</td>
            <td>Decorator、Bridge</td>
        </tr>
        <tr>
            <td>对象创建</td>
            <td>Factory Method、Abstract Factory、Singleton、Builder</td>
        </tr>
        <tr>
            <td>对象性能</td>
            <td>Flyweight、Proxy</td>
        </tr>
        <tr>
            <td>接口隔离</td>
            <td>Facade、Proxy、Adapter、Mediator</td>
        </tr>
        <tr>
            <td>状态变化</td>
            <td>State、Memento</td>
        </tr>
        <tr>
            <td>数据结构</td>
            <td>Composite、Iterato、Chain of Responsibility</td>
        </tr>
        <tr>
            <td>行为变化</td>
            <td>Visitor、Command</td>
        </tr>
        <tr>
            <td>领域问题</td>
            <td>Interpreter</td>
        </tr>
    </tbody>
</table>

<font style="color:red">
    Attention:
</font>

1. 一个设计模式并不是代码，而是某类问题的通用解决方案，设计模式描述了这类问题的解决方案，因此，你可以把它们应用到不同的实际问题中。
2. 重构时构建模式，不要直接使用设计模式，而是先重构代码，然后再应用相应的设计模式，如果你在重构过程中发现某个模式很适合解决重构问题，那么你就可以使用它。
3. 推迟变化的发生

